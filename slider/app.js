// Простий слайдер з Next&Prev
// let offset = 0;
// const sliderLine = document.querySelector(".slider-line");

// document.querySelector(".slider-next").addEventListener("click", () => {
//   offset += 256;
//   if (offset > 768) {
//     offset = 0;
//   }
//   sliderLine.style.left = -offset + "px";
// });
// document.querySelector(".slider-prev").addEventListener("click", () => {
//   offset -= 256;
//   if (offset < 0) {
//     offset = 768;
//   }
//   sliderLine.style.left = -offset + "px";
// });

// Адаптивний слайдер з Next&Prev
window.addEventListener("DOMContentLoaded", () => {
  const images = document.querySelectorAll(".slider .slider-line img");
  const sliderLine = document.querySelector(".slider .slider-line");
  let count = 0;
  let width;

  function init() {
    console.log("resize");
    width = document.querySelector(".slider").offsetWidth;
    sliderLine.style.width = width * images.length + "px";
    images.forEach((item) => {
      item.style.width = width + "px";
      item.style.height = "auto";
    });
    rollSlider();
  }
  init();
  window.addEventListener("resize", init);

  function rollSlider() {
    sliderLine.style.transform = "translate(-" + count * width + "px)";
  }

  document.querySelector(".slider-next").addEventListener("click", () => {
    count++;
    if (count >= images.length) {
      count = 0;
    }
    rollSlider();
  });
  document.querySelector(".slider-prev").addEventListener("click", () => {
    count--;
    if (count < 0) {
      count = images.length - 1;
    }
    rollSlider();
  });
});
